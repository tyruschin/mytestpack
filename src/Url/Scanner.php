<?php
/**
 * Created by PhpStorm.
 * User: tyruschin
 * Date: 16/11/2
 * Time: 上午11:51
 */

namespace Oreilly\ModernPHP\Url;

// src/相当于Oreilly\ModernPHP\
class Scanner{
    protected $urls;
    protected $httpClient;

    public function __construct(array $urls)
    {
        $this->urls = $urls;
        $this->httpClient = new \GuzzleHttp\Client();
    }

    public function getInvalidUrls()
    {
        $invalidUrls = [];
        foreach ($this->urls as $url){
            try {
                $statusCode = $this->getStatusCodeForUrl($url);
            } catch (\Exception $e) {
                $statusCode = 500;
            }
            if($statusCode >= 400) {
                array_push($invalidUrls, [
                    'url' => $url,
                    'status' => $statusCode
                ]);
            }
        }
        return $invalidUrls;
    }

    protected function getStatusCodeForUrl($url)
    {
        $httpResponse = $this->httpClient->options($url);
        return $httpResponse->getStatusCode();
    }
}